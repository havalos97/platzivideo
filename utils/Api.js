const BASE_API = 'https://yts.mx/api/v2';

class API {
	async getSuggestions(id) {
		try {
			const query = await fetch(`${BASE_API}/movie_suggestions.json?movie_id=${id}`);
			const { data } = await query.json();
			return data.movies;
		} catch (error) {
			console.log(error)
		}
	}
};

export default new API();
