import React, {Component} from 'react';
import {
	Text,
} from 'react-native';
import Home from './src/screens/containers/Home.js';
import Header from './src/sections/components/Header.js';
import SuggestionList from './src/videos/containers/SuggestionList.js';
import API from './utils/Api.js';

class App extends Component {
	state = {
		suggestionList: [],
	};

	async componentDidMount() {
		const movies = await API.getSuggestions(10);
		this.setState(prevState => {
			return {
				...prevState,
				suggestionList: movies,
			};
		});
	}

	render() {
		return (
			<Home>
				<Header />
				<Text>Buscador</Text>
				<Text>Categorías</Text>
				<SuggestionList
					list={this.state.suggestionList}
				/>
			</Home>
		);
	}
};

export default App;
