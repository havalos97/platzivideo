import React from 'react';
import {
	View,
	Text,
	Image,
	StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
	},
	leftContainer: {

	},
	rightContainer: {
		paddingLeft: 10,
		justifyContent: 'space-between',
	},
	title: {
		fontSize: 18,
		color: '#44546B',
	},
	categoryContainer: {
		position: 'absolute',
		left: 0,
		top: 0,
		backgroundColor: '#000000',
	},
	categoryText: {
		color: '#FFFFFF',
		fontSize: 11,
		paddingVertical: 5,
		paddingHorizontal: 7,
	},
	year: {
		backgroundColor: '#70B124',
		paddingVertical: 4,
		paddingHorizontal: 6,
		color: '#FFFFFF',
		fontSize: 11,
		borderRadius: 5,
		overflow: 'hidden',
		alignSelf: 'flex-start',
	},
	rating: {
		color: '#6B6B6B',
		fontSize: 14,
		fontWeight: 'bold',
	},
	cover: {
		width: 67,
		height: 100,
		resizeMode: 'contain',
	},
});

const Suggestion = (props) => {
	return (
		<View style={styles.container}>
			<View style={styles.leftContainer}>
				<Image
					source={{
						uri: props.medium_cover_image,
					}}
					style={styles.cover}
				/>
				<View style={styles.categoryContainer}>
					<Text style={styles.categoryText}>{props.genres[0]}</Text>
				</View>
			</View>
			<View style={styles.rightContainer}>
				<Text style={styles.title}>{props.title}</Text>
				<Text style={styles.year}>{props.year}</Text>
				<Text style={styles.rating}>{props.rating}</Text>
			</View>
		</View>
	);
};

export default Suggestion;
