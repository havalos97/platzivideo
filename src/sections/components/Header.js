import React from 'react';
import {
	View,
	Text,
	Image,
	StyleSheet,
	SafeAreaView,
} from 'react-native';

const styles = StyleSheet.create({
	headerLogo: {
		width: 80,
		height: 26,
		resizeMode: 'contain',
	},
	container: {
		padding: 10,
		flexDirection: 'row',
	},
	right: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
	},
});

const Header = (props) => {
	return (
		<View>
			<SafeAreaView>
				<View style={styles.container}>
					<Image
						source={require('../../../assets/logo.png')}
						style={styles.headerLogo}
					/>
					<View style={styles.right}>
						{ props.children }
					</View>
				</View>
			</SafeAreaView>
		</View>
	);
};

export default Header;
