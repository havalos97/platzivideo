import React, {Component} from 'react';
import {
	FlatList,
} from 'react-native';
import Layout from '../components/SuggestionListLayout.js';
import Empty from '../components/Empty.js';
import Separator from '../components/VerticalSeparator.js';
import Suggestion from "../components/Suggestion.js";

class SuggestionList extends Component {
	renderEmpty = () => <Empty text={'No hay sugerencias =('} />;
	itemSeparator = () => <Separator />;
	renderItem = ({ item }) => <Suggestion {...item} />;
	keyExtractor = (item) => item.id.toString();

	render() {
		return (
			<Layout
				title={'Recomendado para ti'}
			>
				<FlatList
					data={this.props.list}
					keyExtractor={this.keyExtractor}
					ListEmptyComponent={this.renderEmpty}
					ItemSeparatorComponent={this.itemSeparator}
					renderItem={this.renderItem}
				/>
			</Layout>
		);
	}
}

export default SuggestionList;
