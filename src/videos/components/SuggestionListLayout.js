import React from 'react';
import {
	View,
	Text,
	StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({
	container: {
		paddingVertical: 10,
		flex: 1,
	},
	title: {
		color: '#4C4C4C',
		fontSize: 20,
		marginBottom: 10,
		marginLeft: 8,
		fontWeight: 'bold',
	},
});

const SuggestionListLayout = (props) => {
	return (
		<View style={styles.container}>
			<Text style={styles.title}>{props.title}</Text>
			{props.children}
		</View>
	);
};

export default SuggestionListLayout;
